import { Resolvers } from "../typesGQL";
import { UpdatePlayerArgs, UpdatePlayerByNameArgs } from "../../model";
import { UpdateMovementArgs } from "../../model/movement.model";
import RuleResolver from "./rule";
export const resolvers: Resolvers = {
  Query: {
    players: (_, _params, { model }) => {
      return model.player.findAll();
    },
    movements: (_, _params, { model }) => {
      return model.movement.findAll();
    },
    rules: (_, _params, { model }) => {
      return model.rule.findAll();
    }
  },
  Mutation: {
    createPlayer: (_, { name }, { model }) => {
      return model.player.create({ name });
    },
    updatePlayer: (_, args, { model }) => {
      return model.player.update(args as UpdatePlayerArgs);
    },
    updatePlayerByName: (_, args, { model }) => {
      return model.player.updateByName(args as UpdatePlayerByNameArgs);
    },
    createMovement: (_, { name }, { model }) => {
      return model.movement.create({ name });
    },
    updateMovement: (_, args, { model }) => {
      return model.movement.update(args as UpdateMovementArgs);
    }
  },
  Rule: RuleResolver
};
