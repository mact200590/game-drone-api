import { Context } from "../../server/context";
import { Rule, RuleResolvers } from "../typesGQL";

export const RuleResolver: RuleResolvers<Context, Rule> = {
  move: ({ move_id }: any, __, { model }) => {
    return model.movement.findOne(move_id);
  },
  kill: ({ kill_id }: any, __, { model }) => {
    return model.movement.findOne(kill_id);
  }
};

export default RuleResolver;
