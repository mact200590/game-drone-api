import { apolloServerInstance } from "./server/apolloServer";
import { log } from "./utils/logging";

apolloServerInstance.listen(process.env.PORT || 4000).then(({ url }) => {
  log("🚀  Server is running on %o", url);
});
