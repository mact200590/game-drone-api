import { ResourceModel } from ".";
import { Repository } from "../repository";
import { Services } from "../services";

interface Player {
  id: string;
  name: string;
  win: string;
  lose: string;
}

export type CreatePlayerArgs = Omit<Player, "id" | "win" | "lose">;

type OptionalExceptFor<T, TRequired extends keyof T> = Partial<T> &
  Pick<T, TRequired>;
export type UpdatePlayerArgs = OptionalExceptFor<Player, "id">;
export type UpdatePlayerByNameArgs = OptionalExceptFor<Player, "name">;

export default class PlayerModel {
  private repository: Repository;
  private services: Services;

  constructor({ repository, services }: ResourceModel) {
    this.repository = repository;
    this.services = services;
  }

  create = (args: CreatePlayerArgs) => {
    return this.repository.player.create(args);
  };

  update = (args: UpdatePlayerArgs) => {
    return this.repository.player.update(args);
  };

  updateByName = (args: UpdatePlayerByNameArgs) => {
    return this.repository.player.updateName(args);
  };

  findAll = () => {
    return this.repository.player.findAll();
  };
}
