import { Repository } from "../repository";
import { Services } from "../services";
import MovementModel from "./movement.model";
import PlayerModel from "./player.model";
import RuleModel from "./rule.model";

export * from "./player.model";

export type ResourceModel = {
  services: Services;
  repository: Repository;
};

export interface Model {
  movement: MovementModel;
  player: PlayerModel;
  rule: RuleModel;
}

export type ContextModel = {
  authorization: string | undefined;
  services: Services;
  repository: Repository;
};

export const modelFactory = ({
  authorization,
  repository,
  services
}: ContextModel): Model => {
  return {
    movement: new MovementModel({ services, repository }),
    player: new PlayerModel({ services, repository }),
    rule: new RuleModel({ services, repository })
  };
};
