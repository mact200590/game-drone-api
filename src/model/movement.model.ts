import { ResourceModel } from ".";
import { Repository } from "../repository";
import { Services } from "../services";

interface Movement {
  id: string;
  name: string;
}

export type CreateMovementArgs = Omit<Movement, "id">;

type OptionalExceptFor<T, TRequired extends keyof T> = Partial<T> &
  Pick<T, TRequired>;
export type UpdateMovementArgs = OptionalExceptFor<Movement, "id">;

export default class MovementModel {
  private repository: Repository;
  private services: Services;

  constructor({ repository, services }: ResourceModel) {
    this.repository = repository;
    this.services = services;
  }

  create = (args: CreateMovementArgs) => {
    return this.repository.movement.create(args);
  };

  update = (args: UpdateMovementArgs) => {
    return this.repository.movement.update(args);
  };

  findAll = () => {
    return this.repository.movement.findAll();
  };

  findOne = async (id: string) => {
    return this.repository.movement.findOne(id);
  };
}
