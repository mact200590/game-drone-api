import bcrypt, { compare } from "bcrypt";

interface CryptService {
  bcryptPassword: (password: string, salt: number) => Promise<string>;
  checkPassword: (
    plainPassword: string,
    cryptedPassword: string
  ) => Promise<boolean>;
}

class CryptServiceImpl implements CryptService {
  bcryptPassword = (password: string, salt: number) => {
    return bcrypt.hash(password, salt);
  };
  checkPassword = (plainPassword: string, cryptedPassword: string) => {
    return compare(plainPassword, cryptedPassword);
  };
}

export default new CryptServiceImpl();
