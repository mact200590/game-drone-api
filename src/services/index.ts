import crypt from "./crypt.service";

export const services = {
  crypt
};

export type Services = typeof services;
