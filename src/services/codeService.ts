import voucherCodes from "voucher-code-generator";

export const randomCodeGenerator = (): string => {
  const code = voucherCodes
    .generate({
      length: 5,
      count: 1,
      charset: voucherCodes.charset("numbers")
    })
    .join();

  return code;
};
