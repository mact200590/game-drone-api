import { Request } from "express";
import { Model, modelFactory } from "../model";
import { repositoryDBFactory } from "../repository";
import { createConnectionGameDB } from "../repository/connectionGameDB";
import { services } from "../services";

interface ContextArgs {
  request: Request;
}

export class Context {
  authorization?: string;
  model: Model;

  constructor({ request }: ContextArgs) {
    this.authorization =
      (request && request.headers["authorization"]) || undefined;
    this.model = modelFactory({
      authorization: this.authorization,
      repository: repositoryDBFactory({
        connection: createConnectionGameDB()
      }),
      services
    });
  }
}
