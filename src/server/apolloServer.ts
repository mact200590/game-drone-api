import { ApolloServer, makeExecutableSchema } from "apollo-server";
import { importSchema } from "graphql-import";
import { resolvers } from "../graphql/resolvers";
import { log } from "../utils/logging";
import { Context as ContextClass } from "./context";

const typeDefs = importSchema(__dirname + "/../graphql/schema/index.graphql");

const schema = makeExecutableSchema({ typeDefs, resolvers: resolvers as any });

const getMocks = () => {
  const args = process.argv.slice(2);
  const mocks = args.find(elem => elem === "--mock") !== undefined;
  if (mocks) {
    log("👻  Mocking Graphql Server");
  }
  return mocks;
};

const configInDev = () => {
  const dev = process.env.NODE_ENV === "dev";
  return {
    playground: dev,
    introspection: dev
  };
};

function apolloServer() {
  const mocks = getMocks();
  const config = configInDev();
  return new ApolloServer({
    schema,
    context: ({ req }) => new ContextClass({ request: req }),
    ...config,
    mocks
  });
}

export const apolloServerInstance = apolloServer();
