import debug from 'debug';

export const log = debug(`${process.env.DEBUG}`);
