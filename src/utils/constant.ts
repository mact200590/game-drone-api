//USER_TYPE
export const USER_TYPE = {
  INTERNAL: "INTERNAL",
  EXTERNAL: "EXTERNAL",
  SYSTEM: "SYSTEM"
};

//ERROR
export const ERROR_SENDING_EMAIL = "ERROR_SENDING_EMAIL";
export const ERROR_AUTHENTICATION = "ERROR_AUTHENTICATION";
export const ERROR_GOOGLE_MAPS = "ERROR_GOOGLE_MAPS";
export const PAGINATION_LIMIT = 20;
export const ERROR_CREATING_PDF = "ERROR_CREATING_PDF";
export const ERROR_USER_NOT_EXIST =
  "El usuario es incorrecto o está desabilitado";
export const ERROR_INVALID_PASSWORD = "La contraseña es incorrecta";
export const ERROR_PERMISSION =
  "Usted no está autorizado para acceder a este recurso";

export const ERROR_NOT_FOUND_CODE = "ERROR_NOT_FOUND_CODE";

//Medium
export const MEDIUM_TYPE = {
  EMAIL: "gmail"
};

//Task
export const TASK_TYPE = {
  COLLECTION_COORDINATE: "030",
  RECONCILIE_CTA_CTE: "035"
};
export const TASK_PRIORITY = {
  MEDIUM: "M",
  HIGH: "A"
};
export const TASK_DESCRIPTION = {
  COLLECTION_COORDINATE: "Llamar para cobrar",
  RECONCILIE_CTA_CTE: "Vincular documentos"
};

export const TASK_STATE = {
  PENDING: "P",
  COMPLETED: "C"
};
