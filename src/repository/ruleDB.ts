import Knex from "knex";
import { GameDBArgs } from ".";

export default class RuleDB {
  private connection: Knex;

  constructor({ connection }: GameDBArgs) {
    this.connection = connection;
  }

  // update = async ({ id, ...rest }: UpdateRuleArgs) => {
  //   return this.connection
  //     .from("rules")
  //     .update({
  //       ...rest
  //     })
  //     .where("id", id)
  //     .then(result => (result ? result !== 0 : false));
  // };

  // create = async ({ name }: CreateRuleArgs) => {
  //   return this.connection
  //     .insert({
  //       name
  //     })
  //     .into("rules")
  //     .then(result => result[0]);
  // };

  findAll = async () => {
    return this.connection
      .select()
      .from("rules")
      .then(rows => rows);
  };
}
