import Knex from "knex";
import PlayerDB from "./gameDB";
import MovementDB from "./movementDB";
import RuleDB from "./ruleDB";

export interface Repository {
  movement: MovementDB;
  player: PlayerDB;
  rule: RuleDB;
}

export type GameDBArgs = {
  connection: Knex;
};

export const repositoryDBFactory = ({
  connection
}: GameDBArgs): Repository => ({
  movement: new MovementDB({ connection }),
  player: new PlayerDB({ connection }),
  rule: new RuleDB({ connection })
});
