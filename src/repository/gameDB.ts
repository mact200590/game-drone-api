import Knex from "knex";
import { GameDBArgs } from ".";
import {
  CreatePlayerArgs,
  UpdatePlayerArgs,
  UpdatePlayerByNameArgs
} from "../model/player.model";

export default class PlayerDB {
  private connection: Knex;

  constructor({ connection }: GameDBArgs) {
    this.connection = connection;
  }

  update = async ({ id, ...rest }: UpdatePlayerArgs) => {
    return this.connection
      .from("players")
      .update({
        ...rest
      })
      .where("id", id)
      .then(result => (result ? result !== 0 : false));
  };

  updateName = async ({ name, ...rest }: UpdatePlayerByNameArgs) => {
    return this.connection
      .from("players")
      .update({
        ...rest
      })
      .where("name", name)
      .then(result => (result ? result !== 0 : false));
  };

  create = async ({ name }: CreatePlayerArgs) => {
    return this.connection
      .insert({
        name,
        win: 0,
        lose: 0
      })
      .into("players")
      .then(result => result[0]);
  };

  findAll = async () => {
    return this.connection
      .select()
      .from("players")
      .then(rows => rows);
  };

  upsert = async (args: CreatePlayerArgs) => {
    const insert = this.connection("players").insert({
      name: args.name,
      win: 0,
      lose: 0
    });

    const rawUpdateParameters = Object.keys(args)
      .map(field => `${field}='${args[field]}'`)
      .join(", ");

    return this.connection.raw("? ON DUPLICATE KEY UPDATE ?", [
      insert,
      this.connection.raw(rawUpdateParameters)
    ]);
  };
}
