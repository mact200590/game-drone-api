import Knex from "knex";
import { GameDBArgs } from ".";
import {
  CreateMovementArgs,
  UpdateMovementArgs
} from "../model/movement.model";

export default class MovementDB {
  private connection: Knex;

  constructor({ connection }: GameDBArgs) {
    this.connection = connection;
  }

  update = async ({ id, ...rest }: UpdateMovementArgs) => {
    return this.connection
      .from("movements")
      .update({
        ...rest
      })
      .where("id", id)
      .then(result => (result ? result !== 0 : false));
  };

  create = async ({ name }: CreateMovementArgs) => {
    return this.connection
      .insert({
        name
      })
      .into("movements")
      .then(result => result[0]);
  };

  findAll = async () => {
    return this.connection
      .select()
      .from("movements")
      .then(rows => rows);
  };

  findOne = async (id: string) => {
    return this.connection
      .select()
      .from("movements")
      .where("id", id)
      .then((rows: any[]) => (rows.length > 0 ? rows[0] : undefined));
  };
}
