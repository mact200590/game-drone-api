import Knex from "knex";

const { DB_HOST, DB_USER, DB_PASSWORD, DB_NAME, DB_PORT } = process.env;

export function createConnectionGameDB() {
  return Knex({
    client: "mysql",
    version: "5.7",
    connection: process.env.DATABASE_URL || {
      host: DB_HOST,
      user: "$2b$10$rArXNMG2yOWSZ3K",
      password: DB_PASSWORD,
      database: DB_NAME,
      port: DB_PORT ? Number.parseInt(DB_PORT) : undefined
    }
  });
}
