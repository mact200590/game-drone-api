const fs = require('fs');
const filePath = 'yarn.lock';

fs.readFile(filePath, 'utf8', function(err, data) {
  if (err) {
    return console.log(err);
  }

  var result = data
    .replace(/http:\/\/polymita.git:4873\//g, 'https://registry.npmjs.org/')
    .replace(/%2f/g, '/');

  fs.writeFile(filePath, result, 'utf8', function(err) {
    if (err) {
      return console.log(err);
    }
  });
});
