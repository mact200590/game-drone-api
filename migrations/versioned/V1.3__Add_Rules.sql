CREATE TABLE `rules` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `move_id` INT NOT NULL,
  `kill_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_move_idx` (`move_id` ASC),
  INDEX `fk_kill_idx` (`kill_id` ASC),
  CONSTRAINT `fk_move`
    FOREIGN KEY (`move_id`)
    REFERENCES `movements` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_kill`
    FOREIGN KEY (`kill_id`)
    REFERENCES `movements` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

