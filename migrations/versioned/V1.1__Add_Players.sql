CREATE TABLE `players` ( 
       `id` int(11) NOT NULL AUTO_INCREMENT,
       `win` int(11) NOT NULL,
       `lose` int(11) NOT NULL,
       `name` varchar(255) NOT NULL,
       
       PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB ROW_FORMAT = Dynamic;