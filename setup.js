const fs = require("fs");
const { promisify } = require("util");

const writeFile = promisify(fs.writeFile);

const envData = `# Generated by setup.js
NODE_ENV="production"
DEBUG="game-api"
APP_SECRET="123*"
DB_HOST="localhost"
DB_USER="root"
DB_PASSWORD=""
DB_NAME=""
DB_PORT=""
BCRYPT_PASSWORD_SALT=12
`;

const flywayData = `# Generated by setup.js
flyway.url=
flyway.user=root
flyway.password=
`;

const configFiles = [{
        filename: ".env",
        content: envData
    },
    {
        filename: "flyway.conf",
        content: flywayData
    }
];

configFiles.forEach(file => {
    if (!fs.existsSync(file.filename)) {
        writeFile(file.filename, file.content, "utf8").catch(err => {
            console.log(err);
        });
    }
});